from common.db import timeseries, risk
from common.config import format_logger
from common.config import age_clusters, mcap_clusters, industry_clusters, sliding_window_sizes
from common.config import maybe_parallel
from common.stats.statsbase import StatsBase
from common.rank_tree import rank_tree
from scipy.stats import linregress
import pandas as pd
import logging


class RiskStats(StatsBase):
    stat_type = 'risk'
    logger = logging.getLogger('risk stats')
    format_logger(logger)

    @classmethod
    def format_stats(cls, date, stats):
        metric_args = []
        benchmark_args = []
        rank_args = []
        # stats =  { time: { cluster: [statz, rank, pctile, summary] } }
        for time, d in stats.items():
            for cluster, dd in d.items():
                cluster_type = cls.cluster_to_cluster_type(cluster)
                statz, rank, percentile, summary = dd
                # first do metrics
                for metric in statz:
                    # benchmarks
                    smr = summary[metric]
                    derived_metric = '%s_%s' % (metric, time)
                    benchmark_args.append(
                        [date, cluster_type, cluster, derived_metric, smr['mean'], smr['std'], smr['min'], smr['25%'],
                         smr['50%'], smr['75%'], smr['max'], smr['count']])
                    for coin in statz.index:
                        value = statz[metric][coin]
                        if 'marketcap' in metric:
                            metric_args.append([time, None, metric, coin, value])
                        else:
                            metric_args.append([time, cluster_type, metric, coin, value])
                # then ranks
                for metric in rank:
                    for coin in rank.index:
                        value = rank[metric][coin]
                        rank_args.append([time, cluster_type, metric, coin, value])
                # then percentile
                for coin in percentile.index:
                    rank_args.append([time, 'global', cls.stat_type, coin, percentile.loc[coin]])
        organized = {x[3]: {'date': date} for x in metric_args}
        for x in metric_args:
            col = cls.get_column_name(x, rank=False)
            name, value = x[3:]
            organized[name][col] = value
        for x in rank_args:
            col = cls.get_column_name(x, rank=True)
            name, value = x[3:]
            organized[name][col] = value

        return organized, benchmark_args

    @classmethod
    def get_stats_for_date(cls, end_date):
        ret = {}
        for t in sliding_window_sizes:
            time = str(t)
            ret[time] = {}
            for age_cluster in age_clusters:
                ret[time][age_cluster] = cls.get_risk_stats(end_date=end_date, days_back=t, age_cluster=age_cluster)
            for mcap_cluster in mcap_clusters:
                ret[time][mcap_cluster] = cls.get_risk_stats(end_date=end_date, days_back=t, mcap_cluster=mcap_cluster)
            for industry_cluster in industry_clusters:
                ret[time][industry_cluster] = cls.get_risk_stats(end_date=end_date, days_back=t, industry_cluster=industry_cluster)
            ret[time]['global'] = cls.get_risk_stats(end_date=end_date, days_back=t)

        return ret

    @classmethod
    def insert(cls, formatted):
        maybe_parallel(risk.insert, formatted.items(), parallel=False)

    @classmethod
    def get_risk_stats(cls, end_date, days_back=0, age_cluster=None, mcap_cluster=None, industry_cluster=None):
        cluster = age_cluster or mcap_cluster or industry_cluster
        cls.logger.debug('Getting risk stats for {}, starting {} days back for cluster {}'.format(end_date, days_back, cluster))
        df = timeseries.get_risk_inputs(end_date=end_date, days_back=days_back, industry_cluster=industry_cluster, age_cluster=age_cluster, mcap_cluster=mcap_cluster)
        pivoted = df.drop_duplicates().pivot("my_date", "token_name")
        # stats has all the coins from the category in it
        stats = cls.get_risk(pivoted)
        current_columns = set(stats.columns)
        desired_columns = set([x.split('_90')[0] for x in rank_tree['root_rank']['risk_rank'].values()])
        missing_columns = desired_columns - current_columns
        unnecessary_columns = current_columns - desired_columns
        if len(missing_columns) > 0:
            cls.logger.warning('Missing desired columns for usage stats: {}'.format(missing_columns))
        if len(unnecessary_columns) > 0:
            cls.logger.debug('Dropping unnecessary columns for risk distribution stats: {}'.format(unnecessary_columns))

        # take rank down index, ranking coins against each other
        rank = stats.rank(axis='index', pct=True)

        # drop the unnecessary columns before computing next level rank
        dropped = rank.drop(inplace=False, axis='columns', columns=unnecessary_columns)

        # overall pctile is mean across all metrics
        pctile = dropped.mean(axis='columns')

        # describe returns quantiles per column (metric in this case)
        summary = stats.describe().fillna(0)

        return stats, rank, pctile, summary

    # ts is a data frame whose symbols constitute the benchmark context
    @classmethod
    def get_risk(cls, ts):
        ts = ts.fillna(0)
        #num_days = len(ts)
        prices_ts = ts.get('price_close', pd.DataFrame([]))
        prices_ts = prices_ts.replace(0, pd.np.nan).interpolate('linear').fillna(method='bfill').dropna(axis=1)
        mcaps_ts = ts.get('marketcap', pd.DataFrame([]))
        mcaps_ts = mcaps_ts.replace(0, pd.np.nan).interpolate('linear').fillna(method='bfill').dropna(axis=1)
        cols = prices_ts.columns.intersection(mcaps_ts.columns)
        prices_ts = prices_ts[cols]
        mcaps_ts = mcaps_ts[cols]
        benchmark_ts = (prices_ts * mcaps_ts).div(mcaps_ts.sum(axis=1), axis="index").sum(axis=1)

        coin_mvmt_pct = prices_ts.pct_change()
        coin_mvmt_pct.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        coin_mvmt_pct.fillna(0, inplace=True)

        benchmark_mvmt_pct = benchmark_ts.pct_change()
        benchmark_mvmt_pct.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        benchmark_mvmt_pct.fillna(0, inplace=True)

        # sharpe / sortino
        excess_returns_pct = coin_mvmt_pct.sub(benchmark_mvmt_pct, axis="index")
        sharpe = excess_returns_pct.mean() / excess_returns_pct.std()
        sharpe.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        sharpe.fillna(0, inplace=True)
        sharpe.name = 'sharpe'

        neg_returns_pct = excess_returns_pct[excess_returns_pct < 0]
        sortino = excess_returns_pct.mean() / neg_returns_pct.std()
        sortino.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        sortino.fillna(0, inplace=True)
        sortino.name = 'sortino'

        # up / down capture
        coin_upside = coin_mvmt_pct[coin_mvmt_pct > 0].sum()
        coin_downside = coin_mvmt_pct[coin_mvmt_pct < 0].sum()
        benchmark_upside = benchmark_mvmt_pct[benchmark_mvmt_pct > 0].sum()
        benchmark_downside = benchmark_mvmt_pct[benchmark_mvmt_pct < 0].sum()

        upside_capture = 100 * coin_upside / benchmark_upside
        upside_capture.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        upside_capture.fillna(0, inplace=True)
        upside_capture.name = 'upside'

        downside_capture = 100 * coin_downside / benchmark_downside
        downside_capture.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        downside_capture.fillna(0, inplace=True)
        downside_capture.name = 'downside'

        up_down_ratio = upside_capture / downside_capture
        up_down_ratio.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        up_down_ratio.fillna(0, inplace=True)
        up_down_ratio.name = 'up_down'

        downside_capture = -1 * downside_capture

        # beta, alpha, r2, treynor
        cov = pd.Series(
            [benchmark_mvmt_pct.cov(coin_mvmt_pct[col]) for col in coin_mvmt_pct.columns],
            index=pd.Index(coin_mvmt_pct.columns)
        )
        cov.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        cov.fillna(0, inplace=True)
        cov.name = 'cov'

        benchmark_var = benchmark_mvmt_pct.var()
        beta = cov / benchmark_var
        beta.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        beta.fillna(0, inplace=True)
        beta.name = 'beta'

        alpha = coin_mvmt_pct.mean() - beta * benchmark_mvmt_pct.mean()
        alpha.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        alpha.fillna(0, inplace=True)
        alpha.name = 'alpha'

        r2 = pd.Series(
            [cls.regress(coin_mvmt_pct[col], benchmark_mvmt_pct) for col in coin_mvmt_pct.columns],
            index=pd.Index(coin_mvmt_pct.columns)
        )
        r2.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        r2.fillna(0, inplace=True)
        r2.name = 'r_squared'

        treynor = excess_returns_pct.mean() / beta
        treynor.replace([pd.np.inf, -pd.np.inf], 0, inplace=True)
        treynor.fillna(0, inplace=True)
        treynor.name = 'treynor'

        mcaps = mcaps_ts.mean(axis='index')
        mcaps.name = 'marketcap'

        risk = pd.concat([upside_capture, downside_capture, up_down_ratio, sharpe, sortino, cov, alpha, beta, r2, treynor, mcaps], axis=1)

        return risk

    @classmethod
    def regress(cls, x, y):
        _, _, r2, _, _ = linregress(x, y)
        return r2 ** 2
