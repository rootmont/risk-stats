from flask import Flask
from flask_api import status
from flask import Response
from apscheduler.schedulers.background import BackgroundScheduler
import datetime
from risk_stats import RiskStats
import time
import logging
from common.config import format_logger, date_interval, earliest_crypto_date


logger = logging.getLogger('main')
format_logger(logger)

app = Flask(__name__)


@app.route('/')
def root():
    return 'I am a teapot.', 420


@app.route('/health',  methods=['GET'])
def health():
    if not scheduler.running or scheduler.get_jobs().__len__() == 0:
        return Response("{}", status=status.HTTP_500_INTERNAL_SERVER_ERROR, mimetype='application/json')
    return Response("{}", status=status.HTTP_200_OK, mimetype='application/json')


def update_risk_stats():

    logger.info('Updating risk stats')
    Risk = RiskStats()
    Risk.update(datetime.date.today())


def backfill_risk_stats():
    Risk = RiskStats()
    yesterday = datetime.date.today() + datetime.timedelta(days=-1)
    # backfill in reverse
    dates = date_interval(start=yesterday, end=earliest_crypto_date, delta=-1)
    # dates = date_interval(earliest_crypto_date, datetime.date.today(), 1)
    for date in dates:
        Risk.update(date)


now = datetime.datetime.utcnow()
jiggle = datetime.timedelta(seconds=15)
if __name__ == '__main__':
    scheduler = BackgroundScheduler()
    scheduler.add_job(
        func=update_risk_stats,
        trigger='interval',
        hours=6,
        max_instances=1,
        next_run_time=now + jiggle
    )
    scheduler.add_job(
        func=backfill_risk_stats,
        trigger='interval',
        hours=6,
        max_instances=1,
        next_run_time = now + jiggle,
    )
    jobs = scheduler.get_jobs()
    logger.debug('Starting scheduler with {} jobs'.format(len(jobs)))
    scheduler.start()
    time.sleep(1)
    try:
        app.run(host='0.0.0.0', port=5000)
    except Exception as e:
        scheduler.shutdown()
